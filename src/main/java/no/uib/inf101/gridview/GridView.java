package no.uib.inf101.gridview;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel{
  private IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid){
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }
  
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);

  }

  private void drawGrid(Graphics2D g2) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D myRect = new Rectangle2D.Double(x, y, width, height);
    g2.setColor(MARGINCOLOR);
    g2.fill(myRect);
    CellPositionToPixelConverter cP = new CellPositionToPixelConverter(myRect, colorGrid,OUTERMARGIN);
    drawCells(g2, colorGrid, cP);
  }

  private static void drawCells(Graphics2D g2d, CellColorCollection cellColors, CellPositionToPixelConverter converter) {
        for (CellColor cell: cellColors.getCells()) {
            if (cell.color() == null) {
              g2d.setColor(Color.DARK_GRAY);
              g2d.fill(converter.getBoundsForCell(cell.cellPosition()));
            } else {
              g2d.setColor(cell.color());
              g2d.fill(converter.getBoundsForCell(cell.cellPosition()));
            }
        }
    }
  
  

}
